# frozen_string_literal: true

class SearchBuilder < Blacklight::SearchBuilder
  include Blacklight::Solr::SearchBuilderBehavior
  include BlacklightRangeLimit::RangeLimitBuilder

  include BlacklightAdvancedSearch::AdvancedSearchBuilder
  include TrlnArgon::ArgonSearchBuilder
  include ArgonCallNumberSearch::SearchBuilderBehavior

  self.default_processor_chain += %i[add_advanced_search_to_solr
                                     add_call_number_query_to_solr]

  ##
  # @example Adding a new step to the processor chain
  #   self.default_processor_chain += [:add_custom_data_to_query]
  #
  #   def add_custom_data_to_query(solr_parameters)
  #     solr_parameters[:custom] = blacklight_params[:user_value]
  #   end

  def exclude_online(solr_parameters)
    solr_parameters[:fq] ||= []
    solr_parameters[:fq] << "-#{TrlnArgon::Fields::ACCESS_TYPE_FACET}:Online"
  end

  def cleaned_bookmark_params
    blacklight_params[:doc_ids].delete('^a-zA-Z0-9|_').gsub('|', ' OR ') # UNCb9249630 OR UNCb9001022
  end
end
